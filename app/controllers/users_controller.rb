class UsersController < ApplicationController

	def profile
		@user = current_user

		@interests = []

		@user.interests.each do |x|
			@interests.push(x.name)
		end
	end	

	def update_avatar
		@user = current_user
		
		if @user.update_attributes(user_params)
	      redirect_to profile_path :notice  => "Successfully updated ."
	    else
	      render :action => 'edit'
	    end

	end

	def edit
		@user = current_user

		@interests = []

		@user.interests.each do |interest|
			@interests.push(interest.name)
		end

		@interests = @interests.join(", ")
	end

	def update
		@user = current_user

		@user.interests.destroy_all

		@interests = []
		params[:user][:interests].split(',').each do |interest|
			if Interest.exists?(:name => interest)
				@user.interests.push(Interest.find_by_name interest)
			else
				@user.interests.push(Interest.create(name: interest))
			end
		end

		@user.name = params[:user][:name]

		@user.avatar = params[:user][:avatar]

		if @user.save
			redirect_to profile_path :notice  => "Successfully updated ."
		else
			render :action => 'edit'
		end
	end	

	def match
		@user = current_user
		@interests = current_user.interests

		@matched_users = []

		@interests.each do |interest|
			users = interest.users
			users.each do |u|
				@matched_users.push(u) if u.id != @user.id
			end
		end

		@matched_users = @matched_users.uniq { |u| u.id }
	end

	def match_me
		@user = current_user
		@interests = current_user.interests

		@matched_users = []

		@interests.each do |interest|
			users = interest.users
			users.each do |u|
				@matched_users.push(u) if u.id != @user.id
			end
		end



		@matched_users = @matched_users.uniq { |u| u.id }
		respond_to do |format|
	      format.js   {}
	      format.json { render json: @matched_users.sample}
	  	end
	end

	def user_params
	  params.require(:user).permit(:avatar, :name, :interests)
	end

end
