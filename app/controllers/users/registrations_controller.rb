module Users
  class RegistrationsController < Devise::RegistrationsController

    def new
      super
    end

    def create
      params[:user][:interests] = params[:user][:interests].split(',');
        
        super
        
        params[:user][:interests].each do |interest|
          if Interest.exists?(:name => interest)
            resource.interests.push(Interest.find_by_name interest)
          else
            resource.interests.push(Interest.create(name: interest))
          end
        end
    end



    private

    def sign_up_params
      params.require(:user).permit(:name, :interests, :avatar, :email, :password, :password_confirmation)
    end

    def account_update_params
      params.require(:user).permit(:name, :avatar, :email, :password, :password_confirmation, :current_password)
    end
  end
end