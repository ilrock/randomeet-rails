$(document).ready(function() {
    
    $(document).keypress(function(event){
        if (event.which == '13') {
            event.preventDefault();
        }
    });

    $('#match-me').click(function(){
		//console.log('click');
		var url = '/match-me'
		$.ajax({
		  dataType: "json",
		  url: url,
		  success: function(data){
		  	$('#match-name').text(data.name);
		  	$('#match-avatar').attr('src', data.avatar);
		  	$('.hidden').removeClass('hidden');
		  }
		});
	})

    
});