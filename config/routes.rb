Rails.application.routes.draw do
  
  root 'pages#home'
  get 'about', to: 'pages#about'
  get 'profile', to: 'users#profile'
  patch 'profile', to: 'users#update_avatar'
  get 'match', to: 'users#match'
  get 'match-me', to: 'users#match_me'
  devise_for :users, :controllers => { :registrations => "users/registrations"}
  resources :users
  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
