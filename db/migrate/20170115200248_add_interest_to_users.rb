class AddInterestToUsers < ActiveRecord::Migration[5.0]
  def change
  	create_table :interests do |t|
      t.string :name
      t.timestamps
    end
 
    create_table :interests_users, id: false do |t|
      t.belongs_to :interest, index: true
      t.belongs_to :user, index: true
    end
  end
end
